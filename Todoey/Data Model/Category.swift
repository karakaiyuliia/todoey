//
//  Category.swift
//  Todoey
//
//  Created by Yuliia Karakai on 2018-12-17.
//  Copyright © 2018 Yuliia Karakai. All rights reserved.
//

import Foundation
import RealmSwift

class Category: Object {
    @objc dynamic var name : String = ""
    @objc dynamic var color : String = ""
    let items = List<Item>()
}
