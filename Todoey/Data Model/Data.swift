//
//  Data.swift
//  Todoey
//
//  Created by Yuliia Karakai on 2018-12-13.
//  Copyright © 2018 Yuliia Karakai. All rights reserved.
//

import Foundation
import RealmSwift

class Data : Object{
    @objc dynamic var name : String = ""
    @objc dynamic var age : Int = 0
}
